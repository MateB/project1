package movies.importer;

import java.util.*;
/**
 * 
 * @author Mate and Alessandro
 *
 */
public class Deduper extends Processor{
	/**
	 * 
	 * @param src folder which contains the original file that will get dedupped
	 * @param dest folder that will receive the dedupped file
	 * method that creates a deduper object
	 */
	public Deduper(String src, String dest) {
		super(src, dest, false);
	}
	/**
	 * @param ArrayList<String> a initial arrayList will all the movies that has yet to be processed
	 * @return newList final version of the arrayList that has been processed and deduped
	 * method that removes all duplicate movies and concatenates the sources if same movie has different sources
	 */
	public ArrayList<String> process(ArrayList<String> array) {

        ArrayList<Movie> mList = new ArrayList<Movie>();
        ArrayList<String> newList = new ArrayList<String>();
        for(int i = 0; i < array.size(); i++) {
            
            String[] splitLine = array.get(i).split("\\t");
            Movie movie = new Movie(splitLine[2], splitLine[0], splitLine[1], splitLine[3]);
            if(!(mList.contains(movie))) {

            	mList.add(movie);
            } 
            else {

                int index = mList.indexOf(movie);
                String arraySource = mList.get(index).getSource();
                if(!(arraySource.equals(movie.getSource()))) {
                    mList.set(index, new Movie(splitLine[2], splitLine[0], splitLine[1], "kaggle;imdb"));
                }
            }
        }
        for(int i = 0; i < mList.size(); i++) {

        	newList.add(mList.get(i).toString());
        }
        return newList;
    }
}
