package movies.importer;

import java.io.*;
/**
 * 
 * @author Mate and Alessandro
 *
 */
public class ImportPipeline {
	/**
	 * The main method fills an array of type processor with 5 empty indices, then fills it with
	 * both the importers, the normalizer validator and deduper, then calls the process all method
	 * with the array as an argument
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException{
		Processor[] mainArray = new Processor[5];
		String url = "C:\\Users\\alexv\\Documents\\Assignments\\Java 3\\project1\\Project_1_MovieImporter\\folder3";
		mainArray[0] = new ImdbImporter("C:\\Users\\alexv\\Documents\\Assignments\\Java 3\\project1\\Project_1_MovieImporter\\folder1", url);
		
		mainArray[1] = new KaggleImporter("C:\\Users\\alexv\\Documents\\Assignments\\Java 3\\project1\\Project_1_MovieImporter\\folder2", url);
		
		
		mainArray[2] = new Normalizer(url, "C:\\Users\\alexv\\Documents\\Assignments\\Java 3\\project1\\Project_1_MovieImporter\\folder4");
		
		
		mainArray[3] = new Validator("C:\\Users\\alexv\\Documents\\Assignments\\Java 3\\project1\\Project_1_MovieImporter\\folder4","C:\\Users\\alexv\\Documents\\Assignments\\Java 3\\project1\\Project_1_MovieImporter\\folder5");
		
		
		mainArray[4] = new Deduper("C:\\Users\\alexv\\Documents\\Assignments\\Java 3\\project1\\Project_1_MovieImporter\\folder5", "C:\\Users\\alexv\\Documents\\Assignments\\Java 3\\project1\\Project_1_MovieImporter\\folder6");
		
		processAll(mainArray);
	}
	
	/**
	 * the processAll method will call the execute method on all indices inside the array.
	 * @param array the array filled with the importers, normalizer, validator and deduper
	 * @throws IOException
	 */
	public static void processAll(Processor[] array) throws IOException {
		for(int i = 0; i < array.length; i++) {
			array[i].execute();
		}
	}
	
}
