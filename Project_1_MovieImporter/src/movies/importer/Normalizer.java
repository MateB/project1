package movies.importer;

import java.util.ArrayList;

public class Normalizer extends Processor
{
	/**
	 * @author Mate Barabas and Alex Vendramin
	 * @param src source folder that contains the files
	 * @param dest destination file that will output a normalized version of the file
	 * creating normalizer object
	 */
	public Normalizer(String src, String dest)
	{
		super(src, dest, false);
	}
	/**
	 * @param ArrayList<String> takes as input an arrayList which is the movies from the source file
	 * @return ArrayList<String> returns a new arrayList that is a normalized version of the movies
	 * method that normalizes the movie by making the title to lower case and keeping only the digits from run time.
	 */
	public ArrayList<String> process(ArrayList<String> a)
	{
		ArrayList<String> movieList = new ArrayList<String>();
		
		for(String s: a)
		{
			String[] arr = s.split("\\t");
			String runTime = arr[1].split(" ")[0];

			Movie movies = new Movie(arr[2],arr[0].toLowerCase(), runTime,arr[3]);
			movieList.add(movies.toString());
		}
		return movieList;
	}
	
}
