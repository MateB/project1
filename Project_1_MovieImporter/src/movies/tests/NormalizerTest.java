package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.*;
/**
 * 
 * @author Mate
 *
 */
class NormalizerTest
{
	/**
	 * Testing the normalizer method to see if it returns the right values and the correct format of having a title in lower case and the digits from the runTime String
	 */
	@Test
	public void processTest() 
	{
		String src = "C:\\Users\\1834578\\Desktop\\java310\\project1\\Project_1_MovieImporter\\KaggleSmallFile.txt";
		String dest = "C:\\Users\\1834578\\Desktop\\project1testKaggle";
        ArrayList<String> list = new ArrayList<String>();
        ArrayList<String> list2 = new ArrayList<String>();
        KaggleImporter ki = new KaggleImporter(src, dest);

        String line1 = "Martina Gedeck\tMoritz Bleibtreu\tJohanna Wokalek\tBruno Ganz\tNadja Uhl\tJan Josef Liefers\tDirector Uli Edel teams with screenwriter Bernd Eichinger to explore a dark period in German history with this drama detailing the rise and fall of the Red Army Faction, a left-wing terrorist organization that became increasingly active following World War II. Also known as the Baader-Meinhof Group, the Red Army Faction was formed by the radicalized children of the Nazi generation with the intended goal of battling Western imperialism and the West German establishment. Adapted from author Stefan Aust's definitive account of the group that resorted to killing innocent civilians in the name of democracy and justice, The Baader Meinhof Complex stars Moritz Bleibtreu as Andreas Baader and Martina Gedeck as Ulrike Meinhof. Bruno Ganz co-stars as Horst Herold, the head of the German police force faced with the task of bringing the Red Army Faction to justice.\tUli Edel\tDirector Not Available\tDirector Not Available\tAction\tR\t9/25/2008\t149 minutes\tVitagraph Films\tDer Baader Meinhof Komplex (The Baader Meinhof Complex)\tUli Edel\tBernd Eichinger\tWriter Not Available\tWriter Not Available\t2008";
        String line2 = "Brett Granstaff\tDiahann Carroll\tLara Jean Chorostecki\tRoddy Piper\tT.J. McGibbon\tJames Preston Rogers\tThe journey of a professional wrestler who becomes a small town pastor and moonlights as a masked vigilante fighting injustice. While facing a crisis at home and at the church, the pastor must evade the police and somehow reconcile his violent secret identity with his calling to be a pastor.\tWarren P. Sonoda\tDirector Not Available\tDirector Not Available\tAction\tPG-13\t1/8/2016\t111 minutes\tFreestyle Releasing\tThe Masked Saint\tScott Crowell\tBrett Granstaff\tWriter Not Available\tWriter Not Available\t2016";


        list2.add(line1);
        list2.add(line2);

        list2 = ki.process(list2);
        String title = "Der Baader Meinhof Komplex (The Baader Meinhof Complex)";
        String runTime = "149 minutes";
        String year = "2008";
        String source = "kaggle";

        Movie newMovie = new Movie(year, title, runTime, source);
        list.add(newMovie.toString());


        assertEquals(list2.get(0), list.get(0));


        title = "The Masked Saint";
        runTime = "111 minutes";
        year = "2016";

        Movie newMovie2 = new Movie(year, title, runTime, source);
        list.add(newMovie2.toString());


        assertEquals(list2.get(1), list.get(1));
	}
	
	

}
