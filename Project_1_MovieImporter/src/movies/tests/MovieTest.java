package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import movies.importer.Movie;
/**
 * 
 * @author Mate and Alessandro
 *
 */
class MovieTest {
	
	/**
	 * testing the movie object
	 */
	@Test
	public void getTest()
	{
		Movie m = new Movie("2011", "The Thing", "1h43", "RottenTomatoes");
		assertEquals("2011", m.getYear());
		assertEquals("The Thing", m.getTitle());
		assertEquals("1h43", m.getRunTime());
		assertEquals("RottenTomatoes", m.getSource());
	}
	/**
	 * testing the toString method
	 */
	@Test
	public void testToString()
	{
		Movie m = new Movie("2011", "The Thing", "103 minutes", "kaggle");
		String expected = "Movie Title: " + "The Thing" + ",	" +"Run Time: " + "103 minutes" + ",	" + "Release Year: " + "2011" + ",	" + "Source: " + "kaggle";
		assertEquals(expected, m.toString());
	}

}
