package movies.importer;

import java.util.*;
/**
 * 
 * @author Alessandro
 *
 */
public class Validator extends Processor{
	
	/**
	 * Validator extends the processor class, so it takes
	 * the source of the file and where you want to put it,
	 * then sets the third value to true so that it will ignore
	 * the first line of the file give, ie: the labels.
	 * @param src src which file to ImdbImporter
	 * @param dest dest which file to store the output for ImdbImporter
	 */
	public Validator(String src, String dest) {
		super(src, dest, false);
	}
	
	/**
	 * the process method in Validator will take and ArrayList
	 * once iterating through the array, it will set a kill variable
	 * to false, if this variable is ever set to true it will not add
	 * the index back into the array once returning. It will set kill
	 * to true if it catches any NullPointerExceptions in any of the first
	 * three indices title, runtime, and release year, or if it catches
	 * a NumberFormatException in runtime or release year.
	 * @return returns the validated ArrayList
	 */
	public ArrayList<String> process(ArrayList<String> array){
		ArrayList<String> newArray = new ArrayList<String>();
		boolean kill;
		for(String s : array) {
			kill = false;
			String[] words = s.split("\\t");
			
			try {
				if(words[2].equals(null) || words[2].equals("")) {
					;
				}
				else {
					;
				}
			}
			catch(NullPointerException e){
				System.out.println("NullPointerException Caught in title");
				kill = true;
			}
			
			
			try {
				if(words[0].equals(null) || words[0].equals("")) {
					;
				}
				else {
					
				}
			}
			catch(NullPointerException e) {
				System.out.println("NullPointerException Caught in release year");
				kill = true;
			}
			
			
			try {
				if(words[1].equals(null)|| words[1].equals("")) {
					;
				}
				else {
					
				}
			}
			catch(NullPointerException e) {
				System.out.println("NullPointerException Caught in run time");
				kill = true;
			}
			
			try {
				if(Integer.parseInt(words[1]) == 0 || Integer.parseInt(words[2]) == 0) {
					;
				}
				else {
					;
				}
			}
			catch(NumberFormatException e){
				System.out.println("NumberFormatException Caught");
				kill = true;
			}
			if(kill == false) {
				newArray.add(s);
			}
			else {
				;
			}
		}
		return newArray;
	}
}
