package movies.importer;

import java.util.ArrayList;
/**
 * 
 * @author Mate
 *
 */
public class KaggleImporter extends Processor 
{
	/**
	 * 
	 * @param src takes as input a source file from a folder
	 * @param dest is where the imported file will be after it has been proccessed
	 * method that creates a kaggleImporter object
	 */
	public KaggleImporter(String src, String dest)
	{
		super(src, dest, true);
	}
	/**
	 * @param ArrayList<String> a arrayList of the source file
	 * @return movieList which is the processed version of the source file that gets sent to the destination file
	 * method that processes the source file and splits the file and creates movies based on the split.
	 */
	public ArrayList<String> process(ArrayList<String> a)
	{
		ArrayList<String> movieList = new ArrayList<String>();
		
		for(String s: a)
		{
			String[] arr = s.split("\\t");
			

			Movie movies = new Movie(arr[20],arr[15],arr[13],"kaggle");
			movieList.add(movies.toString());
		}
		return movieList;
	}

}
