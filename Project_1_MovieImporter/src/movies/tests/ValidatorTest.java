package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.*;

import org.junit.jupiter.api.Test;

import movies.importer.ImdbImporter;
import movies.importer.Validator;
import movies.importer.Movie;
/**
 * 
 * @author Alessandro
 *
 */
class ValidatorTest {
	/**
	 * the method procesTest takes will make sure that the output from the
	 * Validator is correct, hard coding the values we would want from it.
	 */
	@Test
	public void processTest() {
		//this section creates the importer and validator to be used, along with the ArrayLists to be tested
		String src = "C:\\Users\\alexv\\Documents\\Assignments\\Java 3\\project1\\Project_1_MovieImporter\\ImdbSmallFile.txt";
		String dest = "C:\\Users\\alexv\\Desktop\\project1Test";
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<String> list2 = new ArrayList<String>();
		Validator validate = new Validator(src, dest);
		ImdbImporter importer = new ImdbImporter(src, dest);
		
		//this section hard codes lines from the files into some strings
		String line1 = "tt0000009	Miss Jerry	Miss Jerry	1894	1894-10-09	Romance	45	USA	None	Alexander Black	Alexander Black	Alexander Black Photoplays	\"Blanche Bayliss, William Courtenay, Chauncey Depew\"	The adventures of a female reporter in the 1890s.	5.9	154					1	2";
		String line2 = "tt0000574	The Story of the Kelly Gang	The Story of the Kelly Gang	1906	12/26/1906	\"Biography, Crime, Drama\"	70	Australia	None	Charles Tait	Charles Tait	J. and N. Tait	\"Elizabeth Tait, John Tait, Norman Campbell, Bella Cola, Will Coyne, Sam Crewes, Jack Ennis, John Forde, Vera Linden, Mr. Marshall, Mr. McKenzie, Frank Mills, Ollie Wilson\"	True story of notorious Australian outlaw Ned Kelly (1855-80).	6.1	589	\"$2,250 \"	";
		
		//this section adds both lines to an Array list, validates and processes them.
		list2.add(line1);
		list2.add(line2);
		list2 = validate.process(list2);
		list2 = importer.process(list2);
		
		//this section creates the hard coded movie that the output should match, and adds it to an ArrayList
		String title = "Miss Jerry";
		String runTime = "45";
		String year = "1894";
		String source = "imdb";
		Movie newMovie = new Movie(year, title, runTime, source);
		list.add(newMovie.toString());
		
		//this shows that they are still in the ArrayList after validating and processing
		assertEquals(list2.get(0), list.get(0));
		
		
		
		//clear the variables to keep the ArrayLists clean, this process may require more code, but it was easier to keep track of.
		list.clear();
		list2.clear();
		//added a letter into the release year variable, to create a NumberFormatException
		line1 = "tt0000009	Miss Jerry	Miss Jerry	1894a	1894-10-09	Romance	45	USA	None	Alexander Black	Alexander Black	Alexander Black Photoplays	\"Blanche Bayliss, William Courtenay, Chauncey Depew\"	The adventures of a female reporter in the 1890s.	5.9	154					1	2";
		line2 = "tt0000574	The Story of the Kelly Gang	The Story of the Kelly Gang	1906	12/26/1906	\"Biography, Crime, Drama\"	70	Australia	None	Charles Tait	Charles Tait	J. and N. Tait	\"Elizabeth Tait, John Tait, Norman Campbell, Bella Cola, Will Coyne, Sam Crewes, Jack Ennis, John Forde, Vera Linden, Mr. Marshall, Mr. McKenzie, Frank Mills, Ollie Wilson\"	True story of notorious Australian outlaw Ned Kelly (1855-80).	6.1	589	\"$2,250 \"	";
		
		list2.add(line1);
		list2.add(line2);
		
		list2 = validate.process(list2);
		list2 = importer.process(list2);
		
		title = "The Story of the Kelly Gang";
		runTime = "70";
		year = "1906";
		
		Movie newMovie2 = new Movie(year, title, runTime, source);
		list.add(newMovie2.toString());
		
		//this shows that the Validator took out the first index of the ArrayList once it found the NumberFormatException
		assertEquals(list2.get(0), list.get(0));
		
		
		
		list.clear();
		list2.clear();
		//added a letter to the release year of the second index
		line1 = "tt0000009	Miss Jerry	Miss Jerry	1894	1894-10-09	Romance	45	USA	None	Alexander Black	Alexander Black	Alexander Black Photoplays	\"Blanche Bayliss, William Courtenay, Chauncey Depew\"	The adventures of a female reporter in the 1890s.	5.9	154					1	2";
		line2 = "tt0000574	The Story of the Kelly Gang	The Story of the Kelly Gang	1906a	12/26/1906	\"Biography, Crime, Drama\"	70	Australia	None	Charles Tait	Charles Tait	J. and N. Tait	\"Elizabeth Tait, John Tait, Norman Campbell, Bella Cola, Will Coyne, Sam Crewes, Jack Ennis, John Forde, Vera Linden, Mr. Marshall, Mr. McKenzie, Frank Mills, Ollie Wilson\"	True story of notorious Australian outlaw Ned Kelly (1855-80).	6.1	589	\"$2,250 \"	";
		
		list2.add(line1);
		list2.add(line2);
		
		list2 = validate.process(list2);
		list2 = importer.process(list2);
		
		title = "Miss Jerry";
		runTime = "45";
		year = "1894";
		
		Movie newMovie3 = new Movie(year, title, runTime, source);
		list.add(newMovie3.toString());
		
		
		//this shows that the validator took out the second index, since the size is equal to one
		assertEquals(list2.get(0), list.get(0));
		assertEquals(list2.size(), 1);
		
		
		
		
		
		
		list.clear();
		list2.clear();
		//took out the release year of the first index
		line1 = "tt0000009	Miss Jerry	Miss Jerry		1894-10-09	Romance	45	USA	None	Alexander Black	Alexander Black	Alexander Black Photoplays	\"Blanche Bayliss, William Courtenay, Chauncey Depew\"	The adventures of a female reporter in the 1890s.	5.9	154					1	2";
		line2 = "tt0000574	The Story of the Kelly Gang	The Story of the Kelly Gang	1906	12/26/1906	\"Biography, Crime, Drama\"	70	Australia	None	Charles Tait	Charles Tait	J. and N. Tait	\"Elizabeth Tait, John Tait, Norman Campbell, Bella Cola, Will Coyne, Sam Crewes, Jack Ennis, John Forde, Vera Linden, Mr. Marshall, Mr. McKenzie, Frank Mills, Ollie Wilson\"	True story of notorious Australian outlaw Ned Kelly (1855-80).	6.1	589	\"$2,250 \"	";
		
		list2.add(line1);
		list2.add(line2);

		
		list2 = validate.process(list2);
		list2 = importer.process(list2);
		
		title = "The Story of the Kelly Gang";
		runTime = "70";
		year = "1906";
		
		Movie newMovie4 = new Movie(year, title, runTime, source);
		list.add(newMovie4.toString());
		
		//this shows that the validator found the null value in the release year of the first index and removed it
		assertEquals(list2.get(0), list.get(0));
		
		
		
		
		
		list.clear();
		list2.clear();
		//removed the title from the first index
		line1 = "tt0000009			Miss Jerry	1906	1894-10-09	Romance	45	USA	None	Alexander Black	Alexander Black	Alexander Black Photoplays	\"Blanche Bayliss, William Courtenay, Chauncey Depew\"	The adventures of a female reporter in the 1890s.	5.9	154					1	2";
		line2 = "tt0000574	The Story of the Kelly Gang	The Story of the Kelly Gang	1906	12/26/1906	\"Biography, Crime, Drama\"	70	Australia	None	Charles Tait	Charles Tait	J. and N. Tait	\"Elizabeth Tait, John Tait, Norman Campbell, Bella Cola, Will Coyne, Sam Crewes, Jack Ennis, John Forde, Vera Linden, Mr. Marshall, Mr. McKenzie, Frank Mills, Ollie Wilson\"	True story of notorious Australian outlaw Ned Kelly (1855-80).	6.1	589	\"$2,250 \"	";
		
		list2.add(line1);
		list2.add(line2);

		
		list2 = validate.process(list2);
		list2 = importer.process(list2);
		
		title = "The Story of the Kelly Gang";
		runTime = "70";
		year = "1906";
		
		Movie newMovie5 = new Movie(year, title, runTime, source);
		list.add(newMovie5.toString());

		//this shows that the validator found the null value in the title of the first index and removed it
		assertEquals(list2.get(0), list.get(0));
		
		
		
		
		list.clear();
		list2.clear();
		//removed the runtime from the first index
		line1 = "tt0000009	Miss Jerry	Miss Jerry	1906	1894-10-09	Romance		USA	None	Alexander Black	Alexander Black	Alexander Black Photoplays	\"Blanche Bayliss, William Courtenay, Chauncey Depew\"	The adventures of a female reporter in the 1890s.	5.9	154					1	2";
		line2 = "tt0000574	The Story of the Kelly Gang	The Story of the Kelly Gang	1906	12/26/1906	\"Biography, Crime, Drama\"	70	Australia	None	Charles Tait	Charles Tait	J. and N. Tait	\"Elizabeth Tait, John Tait, Norman Campbell, Bella Cola, Will Coyne, Sam Crewes, Jack Ennis, John Forde, Vera Linden, Mr. Marshall, Mr. McKenzie, Frank Mills, Ollie Wilson\"	True story of notorious Australian outlaw Ned Kelly (1855-80).	6.1	589	\"$2,250 \"	";
		
		list2.add(line1);
		list2.add(line2);

		
		list2 = validate.process(list2);
		list2 = importer.process(list2);
		
		title = "The Story of the Kelly Gang";
		runTime = "70";
		year = "1906";
		
		Movie newMovie6 = new Movie(year, title, runTime, source);
		list.add(newMovie6.toString());

		//this shows that the validator found the null value in the runtime of the first index and removed it
		assertEquals(list2.get(0), list.get(0));
	}
}
