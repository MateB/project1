package movies.importer;

public class Movie 
{
	private String releaseYear;
	private String movieTitle;
	private String movieRunTime;
	private String source;

/**
 * 
 * @param releaseYear, string containing release year of movie
 * @param movieTitle, string containing movie title
 * @param movieRunTime, string containing movie run time
 * @param source, string containing the source of the move
 * creating a movie object to store the movies
 */
	public Movie(String releaseYear, String movieTitle, String movieRunTime, String source)
	{
		this.releaseYear = releaseYear;
		this.movieTitle = movieTitle;
		this.movieRunTime = movieRunTime;
		this.source = source;
	}
	/**
	 * 
	 * @return returns the release year
	 * method that whill return the release year of the movie object
	 */
	public String getYear()
	{
		return this.releaseYear;
	}
	/**
	 * 
	 * @return the movie title
	 * method that returns the title of the movie from object
	 */
	public String getTitle()
	{
		return this.movieTitle;
	}
	/**
	 * 
	 * @return run time
	 * method that returns the run time of the movie from object
	 */
	public String getRunTime()
	{
		return this.movieRunTime;
	}
	/**
	 * 
	 * @return source
	 * method that returns the source from the movie object
	 */
	public String getSource()
	{
		return this.source;
	}
	/**
	 * returns a toString version of the movie object
	 */
	public String toString()
	{
		return  getTitle() + "\t" + getRunTime() + "\t" + getYear() + "\t" + getSource();
	}
	
	@Override
    public boolean equals(Object o) {

        if(!(o instanceof Movie)) {

            return false;
        } else {

            Movie movie = (Movie)o;
            if(this.releaseYear.equals(movie.getYear()) && this.movieTitle.equals(movie.getTitle()) && this.movieRunTime.length() != 0 && movie.movieRunTime.length() != 0) {
            	int difference = Integer.parseInt(this.getRunTime()) - Integer.parseInt(movie.getRunTime());
            	if(Math.abs(difference) <= 5) {
            		return true;
            	}
            	else {
            		return false;
            	}
            }
            else {
            	return false;
            }
        }
	}
}
