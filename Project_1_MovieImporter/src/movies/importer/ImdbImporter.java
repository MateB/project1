package movies.importer;
import java.util.*;

public class ImdbImporter extends Processor{
	
	/**
	 * @author Alessandro
	 * ImdbImporter extends the processor class, so it takes
	 * the source of the file and where you want to put it,
	 * then sets the third value to true so that it will ignore
	 * the first line of the file give, ie: the labels.
	 * @param src which file to ImdbImporter
	 * @param dest which file to store the output for ImdbImporter
	 */
	public ImdbImporter(String src, String dest) {
		super(src, dest, true);
	}
	
	/**
	 * The process method in ImdbImporter takes an ArrayList that will have 
	 * long strings of values for a movie, store each of them
	 * into a string, split the string into an array of strings
	 * making a new index at each tab. Then it will return an
	 * ArrayList with only the desired values in a toString format
	 * that identifies the data.
	 * @return returns the imported ArrayList
	 */
	public ArrayList<String> process(ArrayList<String> array){
		ArrayList<String> newArray = new ArrayList<String>();
		for(int i = 0; i < array.size(); i++) {
			String s = array.get(i);
			String[] words = s.split("\\t");
			Movie newMovie = new Movie(words[3], words[2], words[6], "imdb");
			newArray.add(newMovie.toString());	
		}
		return newArray;
	}
}