package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.Movie;

import movies.importer.ImdbImporter;

class ImdbImporterTest {
/**
 * @author Alessandro
 * the method procesTest takes will make sure that the output from the
 * ImdbImporter is correct, hard coding the values we would want from it.
 */
	@Test
	public void processTest() {
		//this section creates the importer to be used with the ArrayLists we will assertEquals upon
		String src = "C:\\Users\\alexv\\Documents\\Assignments\\Java 3\\project1\\Project_1_MovieImporter\\ImdbSmallFile.txt";
		String dest = "C:\\Users\\alexv\\Desktop\\project1Test";
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<String> list2 = new ArrayList<String>();
		ImdbImporter importer = new ImdbImporter(src, dest);
		
		//this section takes some lines from the source file
		String line1 = "tt0000009	Miss Jerry	Miss Jerry	1894	1894-10-09	Romance	45	USA	None	Alexander Black	Alexander Black	Alexander Black Photoplays	\"Blanche Bayliss, William Courtenay, Chauncey Depew\"	The adventures of a female reporter in the 1890s.	5.9	154	";
		String line2 = "tt0000574	The Story of the Kelly Gang	The Story of the Kelly Gang	1906	12/26/1906	\"Biography, Crime, Drama\"	70	Australia	None	Charles Tait	Charles Tait	J. and N. Tait	\"Elizabeth Tait, John Tait, Norman Campbell, Bella Cola, Will Coyne, Sam Crewes, Jack Ennis, John Forde, Vera Linden, Mr. Marshall, Mr. McKenzie, Frank Mills, Ollie Wilson\"	True story of notorious Australian outlaw Ned Kelly (1855-80).	6.1	589	\"$2,250 \"	";

		//this section adds the lines to an ArrayList and processes it
		list2.add(line1);
		list2.add(line2);
		list2 = importer.process(list2);
		
		//this section creates an index in an ArrayList with the values we expect from the processor
		String title = "Miss Jerry";
		String runTime = "45";
		String year = "1894";
		String source = "imdb";
		Movie newMovie = new Movie(year, title, runTime, source);
		list.add(newMovie.toString());
		
		
		assertEquals(list2.get(0), list.get(0));
		
		//make sure it works for both indices
		title = "The Story of the Kelly Gang";
		runTime = "70";
		year = "1906";
		Movie newMovie2 = new Movie(year, title, runTime, source);
		list.add(newMovie2.toString());
		
		
		assertEquals(list2.get(1), list.get(1));
	}

}
